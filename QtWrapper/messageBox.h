#ifndef QTWRAPPER_H
#define QTWRAPPER_H

#include "qtwrapper_global.h"
#include <string>
#include <QString>
#include <QMessageBox>

class QTWRAPPERSHARED_EXPORT messageBox
{
public:
    static void show (std::string text, std::string title = "");
};

#endif // QTWRAPPER_H
