#include "messageBox.h"


void messageBox::show(std::string text, std::string title)
{
    auto box = new QMessageBox();
    box->setText(QString::fromStdString(text));
    box->setWindowTitle(QString::fromStdString(title));
    box->exec();
    delete box;
}
