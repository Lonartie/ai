#ifndef PLUGINMANAGER_H
#define PLUGINMANAGER_H

#include "pluginmanager_global.h"

#include <string>
#include <vector>
#include <map>

#include <QDir>
#include <QLibrary>
#include <QString>
#include <QStringList>

template<class T>
class PLUGINMANAGERSHARED_EXPORT PluginManager
{

public:
    typedef T* (*creationMethod)();

    PluginManager(std::string pluginsFolder)
    {
        auto dir = new QDir(QString::fromStdString(pluginsFolder));
        dir->setFilter(QDir::Files);
    #ifdef _WIN32
        dir->setNameFilters(QStringList() << "*.dll");
    #else
        dir->setNameFilters(QStringList() << "*.dylib");
    #endif
        for (auto file : dir->entryInfoList())
        {
            std::string path = file.absoluteFilePath().toStdString();
            std::string fnName = T::externCreationMethod;
            auto creator = T::getCreator(path);
            auto temp = creator();
            plugins.emplace(temp->getName(), creator);
            delete temp;
        }

        delete dir;
    }

    creationMethod getCreator(std::string name)
    {
        return plugins[name];
    }

    T* getInstance(std::string name)
    {
        return getCreator(name)();
    }

    std::vector<std::string> getNames()
    {
        std::vector<std::string> names;
        for (auto mp : plugins)
            names.push_back(mp.first);
        return names;
    }

    ~PluginManager()
    {
        for (auto item : plugins)
            delete item.second;
        plugins.clear();
        QLibrary::unload();
    }

private:

    std::map<std::string, creationMethod> plugins;

};

#endif // PLUGINMANAGER_H
