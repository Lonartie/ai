#include "acfuncinterface.h"

#include <QLibrary>
#include <QString>

std::string AcFuncInterface::getName()
{
    return "generic";
}

double AcFuncInterface::calc(double input)
{
    return .0;
}

std::string AcFuncInterface::getDescription()
{
    return "generic";
}

AcFuncInterface* AcFuncInterface::getFromLib(std::string path)
{
    return ((createInstance)QLibrary::resolve(QString::fromStdString(path), "createAcFunc"))();
}

AcFuncInterface::createInstance AcFuncInterface::getCreator(std::string path)
{
    return ((createInstance)QLibrary::resolve(QString::fromStdString(path), "createAcFunc"));
}
