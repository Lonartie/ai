#ifndef ACFUNCINTERFACE_H
#define ACFUNCINTERFACE_H

#include <QtCore/qglobal.h>
#include <string>

#include "acfuncinterface_global.h"

class ACFUNCINTERFACESHARED_EXPORT AcFuncInterface
{
public:

    static std::string externCreationMethod;

    typedef AcFuncInterface* (*createInstance)();

    /// @brief return name of this object
    virtual std::string getName();

    /// @brief calculate the output
    /// @param input: double input value
    /// @returns the calculated output
    virtual double calc(double input);

    /// @brief activation functions have to provide a
    /// @brief description
    virtual std::string getDescription();

    /// @brief loads a library from path and returns new
    /// @brief instance of a ac func defined in that lib
    static AcFuncInterface* getFromLib(std::string path);

    static createInstance getCreator(std::string path);

};

std::string AcFuncInterface::externCreationMethod = "createAcFunc";

#define REGISTER_ACFUNC(CLASSNAME)                              \
extern "C"                                                      \
{                                                               \
    Q_DECL_EXPORT CLASSNAME * createAcFunc()             \
    {                                                           \
        return new CLASSNAME ();                                \
    }                                                           \
}

#endif // ACFUNCINTERFACE_H
