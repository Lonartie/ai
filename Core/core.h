#ifndef CORE_H
#define CORE_H

#include "core_global.h"

#include <string>
#include <vector>
#include <functional>

#include "../AiInterface/aiinterface.h"
#include "../CoreData/definitions.h"

class CORESHARED_EXPORT Core
{

public:

    Core();

    std::vector<std::string> getAiNames();

    std::vector<std::string> getAcFuncNames();

    void createAI(std::string name);

    AiStructure getAiStructure();

    void setAiStructure(AiStructure structure);

    AiData run(AiData input);

    AiData train(AiData input, std::function<bool(AiInterface)> evalutaionFunction = [](AiInterface ai){return true;});

};

#endif // CORE_H
