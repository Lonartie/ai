#ifndef PERCEPTRON_H
#define PERCEPTRON_H

#include "perceptron_global.h"
#include "aiinterface.h"
#include <string>

class PERCEPTRONSHARED_EXPORT Perceptron : public AiInterface
{
public:
    Perceptron();
    virtual std::string getName() override;
};

REGISTER_AI(Perceptron)

#endif // PERCEPTRON_H
