#include "acfunc_gaussien_bell_curve.h"

std::string ACFUNC_gaussien_bell_curve::getName()
{
    return "Guassian Bell Curve";
}

double ACFUNC_gaussien_bell_curve::calc(double input)
{
    return exp(-(input * input));
}

std::string ACFUNC_gaussien_bell_curve::getDescription()
{
    return "returns e^(-x^2) also known as the gaussian bell curve..";
}
