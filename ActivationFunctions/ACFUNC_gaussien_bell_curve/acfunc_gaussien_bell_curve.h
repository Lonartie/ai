#ifndef ACFUNC_GAUSSIEN_BELL_CURVE_H
#define ACFUNC_GAUSSIEN_BELL_CURVE_H

#include "acfunc_gaussien_bell_curve_global.h"
#include "acfuncinterface.h"
#include <string>
#include <math.h>

class ACFUNC_GAUSSIEN_BELL_CURVESHARED_EXPORT ACFUNC_gaussien_bell_curve : public AcFuncInterface
{
public:

    virtual std::string getName() override;

    virtual double calc(double input) override;

    virtual std::string getDescription() override;

};

REGISTER_ACFUNC(ACFUNC_gaussien_bell_curve)

#endif // ACFUNC_GAUSSIEN_BELL_CURVE_H
