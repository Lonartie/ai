#-------------------------------------------------
#
# Project created by QtCreator 2018-10-15T17:15:34
#
#-------------------------------------------------

QT       -= gui

TARGET = ACFUNC_gaussien_bell_curve
TEMPLATE = lib

DEFINES += ACFUNC_GAUSSIEN_BELL_CURVE_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#QMAKE_LFLAGS_SONAME  = -Wl,-install_name,@executable_path/../../../../Frameworks/

SOURCES += \
        acfunc_gaussien_bell_curve.cpp

HEADERS += \
        acfunc_gaussien_bell_curve.h \
        acfunc_gaussien_bell_curve_global.h 

unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../AcFuncInterface/release/ -lAcFuncInterface
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../AcFuncInterface/debug/ -lAcFuncInterface
else:unix: LIBS += -L$$OUT_PWD/../../AcFuncInterface/ -lAcFuncInterface

INCLUDEPATH += $$PWD/../../AcFuncInterface
DEPENDPATH += $$PWD/../../AcFuncInterface
