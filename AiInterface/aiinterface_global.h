#ifndef AIINTERFACE_GLOBAL_H
#define AIINTERFACE_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(AIINTERFACE_LIBRARY)
#  define AIINTERFACESHARED_EXPORT Q_DECL_EXPORT
#else
#  define AIINTERFACESHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // AIINTERFACE_GLOBAL_H
