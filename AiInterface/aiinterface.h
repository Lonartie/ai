#ifndef AIINTERFACE_H
#define AIINTERFACE_H

#include <QtCore/qglobal.h>
#include <string>

#include "aiinterface_global.h"

class AIINTERFACESHARED_EXPORT AiInterface
{
public:

    static std::string externCreationMethod;

    typedef AiInterface* (*createInstance)();

    virtual std::string getName();

    static AiInterface* getFromLib(std::string path);
};

 std::string AiInterface::externCreationMethod = "createAI";

#define REGISTER_AI(CLASS_NAME)                    \
extern "C"                                         \
{                                                  \
    Q_DECL_EXPORT CLASS_NAME * createAI()          \
    {                                              \
        return new CLASS_NAME ();                  \
    }                                              \
}

#endif // AIINTERFACE_H
