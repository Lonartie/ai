#include "aiinterface.h"

#include <QLibrary>
#include <QString>

AiInterface* AiInterface::getFromLib(std::string path)
{
    return ((createInstance)QLibrary::resolve(QString::fromStdString(path), "createAI"))();
}

std::string AiInterface::getName()
{
    return "generic";
}
