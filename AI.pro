TEMPLATE = subdirs

SUBDIRS =\
    Core\
    AI_GUI \
    AiInterface \
    AcFuncInterface \
    ActivationFunctions \
    PluginManager \
    QtWrapper \
    CoreData \
    AIs

AI_GUI.depends = Core PluginManager AcFuncInterface AiInterface QtWrapper
Core.depends = AiInterface AcFuncInterface PluginManager CoreData
AiInterface.depends = CoreData
