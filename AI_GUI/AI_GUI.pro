#-------------------------------------------------
#
# Project created by QtCreator 2018-10-14T00:21:52
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AI_GUI
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

QMAKE_LFLAGS_SONAME  = -Wl,-install_name,@executable_path/../Frameworks/

SOURCES += \
        main.cpp \
        mainwindow.cpp

HEADERS += \
        mainwindow.h

FORMS += \
        mainwindow.ui


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../AiInterface/release/ -lAiInterface
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../AiInterface/debug/ -lAiInterface
else:unix: LIBS += -L"../AiInterface/" -lAiInterface

INCLUDEPATH += $$PWD/../AiInterface
DEPENDPATH += $$PWD/../AiInterface

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../AcFuncInterface/release/ -lAcFuncInterface
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../AcFuncInterface/debug/ -lAcFuncInterface
else:unix: LIBS += -L$$OUT_PWD/../AcFuncInterface/ -lAcFuncInterface

INCLUDEPATH += $$PWD/../AcFuncInterface
DEPENDPATH += $$PWD/../AcFuncInterface

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../PluginManager/release/ -lPluginManager
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../PluginManager/debug/ -lPluginManager
else:unix: LIBS += -L$$OUT_PWD/../PluginManager/ -lPluginManager

INCLUDEPATH += $$PWD/../PluginManager
DEPENDPATH += $$PWD/../PluginManager

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Core/release/ -lCore
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Core/debug/ -lCore
else:unix: LIBS += -L$$OUT_PWD/../Core/ -lCore

INCLUDEPATH += $$PWD/../Core
DEPENDPATH += $$PWD/../Core

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../QtWrapper/release/ -lQtWrapper
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../QtWrapper/debug/ -lQtWrapper
else:unix: LIBS += -L$$OUT_PWD/../QtWrapper/ -lQtWrapper

INCLUDEPATH += $$PWD/../QtWrapper
DEPENDPATH += $$PWD/../QtWrapper
