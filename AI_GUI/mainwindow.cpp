#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>
#include <QPushButton>
#include <QFileDialog>
#include <QDir>

#include "aiinterface.h"
#include "acfuncinterface.h"
#include "pluginmanager.h"
#include "messageBox.h"

static const std::string pluginsPath = "/Users/leongierschner/Dropbox/Qt/Plugins";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->pushButton, &QPushButton::pressed, this, &MainWindow::btPressed);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::btPressed()
{

}
